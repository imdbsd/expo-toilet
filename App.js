import React from 'react'
import AppNavigation from './src/components/AppNavigation'

export default function App() {
  return <AppNavigation />
}
