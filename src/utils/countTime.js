function countTime(date) {
  const today = new Date()
  const target = new Date(date)

  const difference = Math.abs(target - today)

  const minuteDifference = Math.round(
    ((difference % 86400000) % 3600000) / 60000
  )
  if (minuteDifference <= 60) {
    return {
      difference: minuteDifference,
      units: 'minutes',
      text: `${minuteDifference} min`,
    }
  }
  const hourDifference = Math.floor((difference % 86400000) / 3600000)
  if (hourDifference <= 24) {
    return {
      difference: hourDifference,
      units: 'hours',
      text: `${hourDifference} hrs`,
    }
  }

  const dayDifference = Math.floor(difference / 86400000)
  return {
    difference: dayDifference,
    units: 'days',
    text: `${dayDifference} days`,
  }
}

export default countTime
