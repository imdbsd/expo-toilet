function cutString(content, max) {
  if (content.length > max) {
    return content.substring(0, max) + '...'
  }
  return content
}

export default cutString
