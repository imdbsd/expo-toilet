import React, { Component } from 'react'
import Constants from 'expo-constants'
import { loadAsync } from 'expo-font'
import { View, Text, ScrollView, Image, StyleSheet } from 'react-native'
import { FormLabel } from 'react-native-elements'
import { Dimensions } from 'react-native'
import Header from '../components/Header'
import BallLoading from '../../assets/loading-ball.gif'
import countTime from '../utils/countTime'
import shitsImage from '../../assets/shits.png'

const { width } = Dimensions.get('screen')

class ReadArticle extends Component {
  state = {
    isLoading: true,
  }
  componentDidMount() {
    loadAsync({
      'merri-weather-bold': require('../../assets/fonts/Merriweather/Merriweather-Bold.ttf'),
      'merri-weather-regular': require('../../assets/fonts/Merriweather/Merriweather-Regular.ttf'),
    }).then(() => this.setState({ isLoading: false }))
  }
  render() {
    // console.log(this.props)
    const article = this.props.navigation.getParam('article')
    console.log({ article })
    return (
      <View style={{ paddingTop: Constants.statusBarHeight }}>
        <Header navigation={this.props.navigation} />
        {this.state.isLoading && (
          <View style={{ textAlign: 'center' }}>
            <Image source={BallLoading} />
            <Text>Loading...</Text>
          </View>
        )}
        {!this.state.isLoading && (
          <ScrollView>
            <Image
              source={{ uri: article.urlToImage }}
              style={{ width: width, height: 200 }}
            />
            <View style={{ padding: 10 }}>
              <Text style={styles.title}>{article.title}</Text>
            </View>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                padding: 10,
              }}
            >
              <Image
                source={shitsImage}
                style={{
                  height: 15,
                  width: 10,
                  resizeMode: 'contain',
                  marginRight: 10,
                }}
              />
              <Text>by Gaben {countTime(article.publishedAt).text} ago</Text>
            </View>
            <View style={{ padding: 10 }}>
              <Text>{article.content}</Text>
            </View>
          </ScrollView>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'merri-weather-bold',
    fontSize: 18,
  },
})

export default ReadArticle
