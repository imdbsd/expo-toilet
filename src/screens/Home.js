import React, { Component } from 'react'
import Constants from 'expo-constants'
import { View, Text } from 'react-native'
import Header from '../components/Header'
import ArticleList from '../components/ArticleList'

class Home extends Component {
  render() {
    return (
      <View style={{ paddingTop: Constants.statusBarHeight, flex: 1 }}>
        <Header navigation={this.props.navigation} />
        <ArticleList navigation={this.props.navigation} />
      </View>
    )
  }
}

export default Home
