import React from 'react'
import {
  View,
  StyleSheet,
  Image,
  Text,
  ScrollView,
  TouchableOpacity,
} from 'react-native'
import { FlatList } from 'react-navigation'
import Constants from 'expo-constants'
import HamburgerBackclose from '../../assets/HamburgerBackclose.png'

function Siderbar(props) {
  return (
    <View style={styles.container}>
      <View
        style={{
          padding: 10,
          alignItems: 'flex-end',
        }}
      >
        <TouchableOpacity onPress={() => props.navigation.closeDrawer()}>
          <Image
            source={HamburgerBackclose}
            style={{ height: 30, resizeMode: 'contain' }}
          />
        </TouchableOpacity>
      </View>
      <FlatList
        style={{ flex: 1 }}
        data={new Array(30).fill(0)}
        renderItem={({ index }) => (
          <Text
            key={index}
            style={styles.content}
            onPress={() => console.log('pres')}
          >
            Topic - {index}
          </Text>
        )}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingTop: Constants.statusBarHeight,
    flex: 1,
  },
  content: {
    padding: 10,
  },
  close: {
    position: 'absolute',
    right: 10,
  },
})

export default Siderbar
