import React, { Fragment } from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Text } from 'react-native'
import mainLogo from '../../assets/main-logo.png'
import hamburger from '../../assets/Hamburger.png'

function Header(props) {
  return (
    <Fragment>
      <View style={styles.container}>
        <Image source={mainLogo} />
        <TouchableOpacity
          onPress={() => props.navigation.openDrawer()}
          style={styles.hamburgerWrapper}
        >
          <Image source={hamburger} style={styles.hamburger} />
        </TouchableOpacity>
      </View>
      <View style={styles.greyBar} />
    </Fragment>
  )
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 15,
    backgroundColor: 'white',
    resizeMode: 'contain',
    elevation: 5,
  },
  hamburgerWrapper: {
    position: 'absolute',
    top: 15,
    left: 10,
  },
  hamburger: {
    height: 30,
    overflow: 'visible',
    resizeMode: 'contain',
  },
  greyBar: {
    backgroundColor: '#e5e5e5',
    height: 10,
  },
})

export default Header
