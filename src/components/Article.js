import React from 'react'
import * as Font from 'expo-font'
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native'
import cutString from '../utils/cutString'
import countTime from '../utils/countTime'
import shitsImage from '../../assets/shits.png'

function Article(props) {
  console.log({ props })
  const { article, navigation } = props
  const [isFontLoaded, setLoaded] = React.useState(false)

  React.useEffect(() => {
    Font.loadAsync({
      'merri-weather-bold': require('../../assets/fonts/Merriweather/Merriweather-Bold.ttf'),
      'merri-weather-regular': require('../../assets/fonts/Merriweather/Merriweather-Regular.ttf'),
    }).then(() => setLoaded(true))
  })
  if (isFontLoaded) {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => navigation.navigate('ReadArticle', { article })}
      >
        <View
          style={{
            ...styles.containerHalf,
            paddingVertical: 10,
          }}
        >
          <Text style={styles.title}>{cutString(article.title, 25)}</Text>
          <Text style={styles.content}>
            {cutString(`${article.content}`, 50)}
          </Text>
          <View style={styles.authorView}>
            <Image source={shitsImage} style={styles.shitIcon} />
            <Text>
              {article.author !== null
                ? cutString(article.author, 10)
                : 'anonymous'}{' '}
              &nbsp;
              {countTime(article.publishedAt).text} ago
            </Text>
          </View>
        </View>
        <View style={styles.containerHalf}>
          <Image
            source={{ uri: article.urlToImage }}
            style={styles.articleImage}
          />
        </View>
      </TouchableOpacity>
    )
  }
  return null
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
  },
  title: {
    fontSize: 18,
    fontFamily: 'merri-weather-bold',
  },
  articleImage: {
    width: '100%',
    height: 100,
    borderRadius: 5,
  },
  content: {
    fontFamily: 'merri-weather-regular',
    fontSize: 12,
    color: '#c3c3c3',
  },
  containerHalf: {
    flex: 1,
    padding: 5,
  },
  authorView: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 10,
  },
  shitIcon: {
    height: 15,
    width: 10,
    resizeMode: 'contain',
    marginRight: 10,
  },
})

export default Article
