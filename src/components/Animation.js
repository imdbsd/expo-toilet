import React from 'react'
import Svg, { Circle, animate } from 'react-native-svg'

const BallLoading = props => (
  <Svg
    width={200}
    height={200}
    viewBox="0 0 100 100"
    preserveAspectRatio="xMidYMid"
    className="prefix__lds-ball"
    style={{
      background: '0 0',
    }}
    {...props}
  >
    <Circle cx={50} cy={23.949} r={13} fill="#c5523f">
      <animate
        attributeName="cy"
        calcMode="spline"
        values="23;77;23"
        keyTimes="0;0.5;1"
        dur={1}
        keySplines="0.45 0 0.9 0.55;0 0.45 0.55 0.9"
        begin="0s"
        repeatCount="indefinite"
      />
    </Circle>
  </Svg>
)

export default BallLoading
