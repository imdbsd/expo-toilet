import React from 'react'
import {
  createStackNavigator,
  createDrawerNavigator,
  createAppContainer,
} from 'react-navigation'
import { Dimensions } from 'react-native'
import Sidebar from './Sidebar'
import Home from '../screens/Home'
import ReadArticle from '../screens/ReadArticle'

const { width } = Dimensions.get('screen')

const AppStack = createStackNavigator(
  {
    Home: Home,
    ReadArticle: ReadArticle,
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
  }
)

const AppDrawer = createDrawerNavigator(
  {
    AppStack: AppStack,
  },
  {
    contentComponent: props => <Sidebar {...props} />,
    drawerWidth: width,
  }
)

export default createAppContainer(AppDrawer)
